package com.webmastersfactory.cardbag.holders

import android.content.Intent
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.activities.ContactDetailActivity
import com.webmastersfactory.cardbag.activities.QRActivity
import com.webmastersfactory.cardbag.models.Contact

class PersonalContactCardHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
    private val emailTextview: TextView = itemView.findViewById(R.id.txtemail)
    private val namesTextview: TextView = itemView.findViewById(R.id.names)
    private val phoneTextView: TextView = itemView.findViewById(R.id.txtphone)
    private val shareImageView: AppCompatImageView = itemView.findViewById(R.id.share)
    private val contactImageView: AppCompatImageView = itemView.findViewById(R.id.contact_image)

    private var currentContact: Contact? = null

    init {
        itemView.setOnClickListener{
            val intent = Intent(itemView.context, ContactDetailActivity::class.java)
            val contactJson = Gson().toJson(currentContact)
            intent.putExtra(Constants.contact,contactJson)
            intent.putExtra(Constants.is_owner,true)
            itemView.context.startActivity(intent)
        }
        shareImageView.setOnClickListener{
            val intent = Intent(itemView.context, QRActivity::class.java)
            intent.putExtra(Constants.contact_id,currentContact?.id)
            itemView.context.startActivity(intent)
        }
    }

    fun updateWithContact(contact: Contact){
        currentContact = contact
        namesTextview.text = contact.names
        emailTextview.text = contact.email
        phoneTextView.text = contact.phone

        if(contact.photo_url != null){
            Picasso.get()
                .load(contact.photo_url)
                .resize(60, 60)
                .centerCrop()
                .into(contactImageView)
        }
    }


}