package com.webmastersfactory.cardbag.holders

import android.content.Intent
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.Gson
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.activities.ContactDetailActivity
import com.webmastersfactory.cardbag.models.Contact
import com.squareup.picasso.Picasso

class ContactHolder (itemView: View) :RecyclerView.ViewHolder(itemView){
    private val contactImageView: ImageView = itemView.findViewById<AppCompatImageView>(R.id.contact_image)
    private val initialTextview: TextView = itemView.findViewById(R.id.initial)
    private val namesTextview: TextView = itemView.findViewById(R.id.names)

    private var currentContact: Contact? = null

    init {
        itemView.setOnClickListener{
            val intent = Intent(itemView.context,ContactDetailActivity::class.java)
            val contactJson = Gson().toJson(currentContact)
            intent.putExtra(Constants.contact,contactJson)
            intent.putExtra(Constants.is_owner,false)
            itemView.context.startActivity(intent)
        }
    }

    fun updateWithContact(contact: Contact){
        currentContact = contact
        if(contact.photo_url != null){
            Picasso.get()
                .load(contact.photo_url)
                .resize(60, 60)
                .centerCrop()
                .into(contactImageView)
        }
        if(contact.names != null){
            initialTextview.text = contact.names[0].toString()
        }
        namesTextview.text = contact.names

    }


}