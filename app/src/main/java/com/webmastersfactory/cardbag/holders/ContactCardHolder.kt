package com.webmastersfactory.cardbag.holders

import android.content.Intent
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.activities.ContactDetailActivity
import com.webmastersfactory.cardbag.activities.QRActivity
import com.webmastersfactory.cardbag.models.Contact

class ContactCardHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
    private val emailTextview: TextView = itemView.findViewById(R.id.txtemail)
    private val namesTextview: TextView = itemView.findViewById(R.id.names)
    private val companyTextView: TextView = itemView.findViewById(R.id.txtcompany)
    private val occupationTextview: TextView = itemView.findViewById(R.id.occupation)
    private val postalTextview: TextView = itemView.findViewById(R.id.txtpostaladdress)
    private val physicalTextView: TextView = itemView.findViewById(R.id.txtphysicaladdress)
    private val websiteTextView: TextView = itemView.findViewById(R.id.txtwebsite)
    private val phoneTextView: TextView = itemView.findViewById(R.id.txtphone)
    private val shareImageView: AppCompatImageView = itemView.findViewById(R.id.share)
    private val contactImageView: AppCompatImageView = itemView.findViewById(R.id.contact_image)

    private var currentContact: Contact? = null

    init {
        itemView.setOnClickListener{
            val intent = Intent(itemView.context, ContactDetailActivity::class.java)
            val contactJson = Gson().toJson(currentContact)
            intent.putExtra(Constants.contact,contactJson)
            intent.putExtra(Constants.is_owner,true)
            itemView.context.startActivity(intent)
        }
        shareImageView.setOnClickListener{
            val intent = Intent(itemView.context, QRActivity::class.java)
            intent.putExtra(Constants.contact_id,currentContact?.id)
            itemView.context.startActivity(intent)
        }
    }

    fun updateWithContact(contact: Contact){
        currentContact = contact
        namesTextview.text = contact.names
        occupationTextview.text = contact.occupation
        emailTextview.text = contact.email
        websiteTextView.text = contact.website
        phoneTextView.text = contact.phone
        postalTextview.text = contact.postal_address
        physicalTextView.text = contact.physical_address
        companyTextView.text = contact.company

        if(contact.photo_url != null){
            Picasso.get()
                .load(contact.photo_url)
                .resize(60, 60)
                .centerCrop()
                .into(contactImageView)
        }
    }


}