package com.webmastersfactory.cardbag.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.holders.ContactCardHolder
import com.webmastersfactory.cardbag.holders.PersonalContactCardHolder
import com.webmastersfactory.cardbag.models.Contact

class ContactCardAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    val contactlist : ArrayList<Contact> = ArrayList()
    private val TYPE_PERSONAL = 0
    private val TYPE_BUSINESS = 1


    override fun getItemCount(): Int {
        return contactlist.size
    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val contact = contactlist[position]

        val itemType  = getItemViewType(position)

        if (itemType == TYPE_PERSONAL) {

            (holder as PersonalContactCardHolder).updateWithContact(contact)

        } else {

            (holder as ContactCardHolder).updateWithContact(contact)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == TYPE_PERSONAL) {
            val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.personal_contact_card, parent, false)
            PersonalContactCardHolder(layoutView)
        } else {
            val contactItem = LayoutInflater.from(parent.context).inflate(R.layout.contact_card,parent,false)

            ContactCardHolder(contactItem)
        }
    }


    override fun getItemViewType(position: Int): Int {
        val contactType = contactlist[position].type
        return if (contactType == "Personal")
            TYPE_PERSONAL
        else {
            TYPE_BUSINESS
        }
    }



}