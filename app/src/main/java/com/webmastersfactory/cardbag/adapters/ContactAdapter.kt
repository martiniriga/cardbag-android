package com.webmastersfactory.cardbag.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.holders.ContactHolder
import com.webmastersfactory.cardbag.models.Contact

class ContactAdapter: RecyclerView.Adapter<ContactHolder>(){

    val contactlist : ArrayList<Contact> = ArrayList()

    override fun getItemCount(): Int {
       return contactlist.size
    }
    override fun onBindViewHolder(holder: ContactHolder, position: Int) {
        var contact = contactlist[position]

        holder.updateWithContact(contact)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactHolder {

        var contactItem = LayoutInflater.from(parent.context)
                .inflate(R.layout.contact_row,parent,false)

        return ContactHolder(contactItem)
    }
}