package com.webmastersfactory.cardbag.fragments


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson

import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.activities.SearchActivity
import com.webmastersfactory.cardbag.adapters.ContactAdapter
import com.webmastersfactory.cardbag.models.ContactQueryData
import com.webmastersfactory.cardbag.models.Urls
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_contacts.*
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset


class ContactsFragment : Fragment() {

    var search_card_view: CardView? = null
    var contacts_recycler : RecyclerView? = null
    var adapter: ContactAdapter = ContactAdapter()
    var prefs: SharedPreferences? = null
    var refresher:SwipeRefreshLayout? = null
    var noContacts: RelativeLayout? = null



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_contacts, container, false)
        prefs = PreferenceHelper.defaultPrefs(context!!)
        search_card_view = view.findViewById(R.id.search_card_view)
        contacts_recycler = view.findViewById(R.id.contacts_recycler)
        noContacts = view.findViewById(R.id.noContacts)
        refresher = view.findViewById(R.id.refresher)

        activity!!.toolbar.setTitle(R.string.contacts)

        search_card_view!!.setOnClickListener {
            val searchIntent = Intent(context, SearchActivity::class.java)
            context!!.startActivity(searchIntent)
        }

        contacts_recycler!!.layoutManager = LinearLayoutManager(context)
        contacts_recycler!!.adapter = adapter

        refresher?.setOnRefreshListener {
            getContacts()
        }

        getContacts()

        return view
    }

    private fun getContacts(){
        refresher?.isRefreshing = true
        Log.d(tag, "Site Info token" + prefs!!.getString(Constants.token,Constants.token))
        val stringRequest = object : JsonObjectRequest(
            Request.Method.GET,  Urls.getUrl(Urls.contacts_list), null,
            Response.Listener { response ->
                activity!!.runOnUiThread {
                    refresher?.isRefreshing = false
                }
                try {
                    val gson = Gson()
                    val contactData: ContactQueryData = gson.fromJson(response.toString(),  ContactQueryData::class.java)
                    adapter.contactlist.clear()
                    adapter.contactlist.addAll(contactData.data)
                    activity!!.runOnUiThread {
                        if(adapter.contactlist.isEmpty()){
                            noContacts!!.visibility = View.VISIBLE
                            contacts_recycler!!.visibility = View.INVISIBLE
                        }else{
                            noContacts!!.visibility = View.INVISIBLE
                            contacts_recycler!!.visibility = View.VISIBLE
                            adapter.notifyDataSetChanged()
                        }
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }, Response.ErrorListener { error ->
                activity!!.runOnUiThread { refresher?.isRefreshing = false }
                val response = error.networkResponse
                if (response != null) {
                    try {
                        val res = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        Log.d("Response Error:", res)
                        val obj = JSONObject(res)
                        val message = obj.getString("message")
                        context!!.showToast(message)
                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()
                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                    }
                }

            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): HashMap<String, String> {
                return hashMapOf(
                    "Authorization" to "Bearer "+ prefs!!.getString(Constants.token,Constants.token),
                    "Content-Type" to "application/json"
                )
            }
        }

        VolleyService.requestQueue.add(stringRequest)
    }


}
