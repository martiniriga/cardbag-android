package com.webmastersfactory.cardbag.fragments


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.google.zxing.integration.android.IntentIntegrator

import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.activities.MainActivity
import com.webmastersfactory.cardbag.models.Urls
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_scan.*
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset


class ScanFragment : Fragment() {
    var prefs: SharedPreferences? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        prefs = PreferenceHelper.defaultPrefs(context!!)
        val view = inflater.inflate(R.layout.fragment_scan, container, false)
        activity!!.toolbar.setTitle(R.string.scan_qr)
        val textSuccess = view.findViewById<TextView>(R.id.textSuccess)
        textSuccess.setOnClickListener { scanBarcode() }
        return view
    }


    //-------------------------Method to scan barcode----------------------------------//
    private fun scanBarcode() {
        //Use IntentIntegrator to launch the intent with the default options:
        val integrator = IntentIntegrator.forSupportFragment(this)//IntentIntegrator(activity!!)
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
        integrator.setPrompt("Please focus the camera on the QR Code")
        integrator.setCameraId(0)  // Use a specific camera of the device
        integrator.setBeepEnabled(false)
        integrator.setBarcodeImageEnabled(true)
        integrator.initiateScan()
    }//-------------------------./Method to scan barcode----------------------------------//


    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        val scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent)
        if (scanResult != null) {
            val resultString = scanResult.contents
            if (resultString != null) {
                Log.d("Response scan: ", resultString)
                sendToken(resultString)
            }
        }else{
            super.onActivityResult(requestCode, resultCode, intent)
        }
        // else continue with any other code you need in the method

    }

    private fun sendToken(resultString: String) {
        val par = activity as MainActivity
        par.showProgressDialog()
        val params = HashMap<String,String>()
        params.put("token", resultString)

        val stringRequest = object : JsonObjectRequest(
              Urls.getUrl(Urls.add_contact), JSONObject(params),
            Response.Listener { response ->
                try {
                    try {
                        val obj = JSONObject(response.toString())
                        val message: String = obj.getString("message")

                        activity!!.runOnUiThread{
                            par.hideProgressDialog()
                            context!!.showToast(message)
                            val intent = Intent(context, MainActivity::class.java)
                            startActivity(intent)
                            activity!!.finish()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }, Response.ErrorListener { error ->
                val response = error.networkResponse
                if (response != null) {
                    try {
                        val res = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        Log.d("Response Error", res)
                        val obj = JSONObject(res)
                        val message = obj.getString("message")
                        activity!!.runOnUiThread {
                            context!!.showToast(message)
                        }
                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()
                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                    }
                }

            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return hashMapOf(
                    "Authorization" to "Bearer " + prefs!!.getString(Constants.token, Constants.token),
                    "Content-Type" to "application/json"
                )
            }
        }
        VolleyService.requestQueue.add(stringRequest)
    }


}
