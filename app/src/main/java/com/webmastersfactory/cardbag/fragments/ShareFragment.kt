package com.webmastersfactory.cardbag.fragments



import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson

import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.activities.CreateContactActivity
import com.webmastersfactory.cardbag.adapters.ContactCardAdapter
import com.webmastersfactory.cardbag.models.ContactQueryData
import com.webmastersfactory.cardbag.models.Urls
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset


class ShareFragment : Fragment() {
    var contacts_recycler : RecyclerView? = null
    var adapter: ContactCardAdapter = ContactCardAdapter()
    var prefs: SharedPreferences? = null
    var refresher: SwipeRefreshLayout? = null
    var noCards: RelativeLayout? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_share, container, false)
        contacts_recycler = view.findViewById(R.id.contacts_recycler)
        noCards = view.findViewById(R.id.noCards)
        refresher = view.findViewById(R.id.refresher)

        contacts_recycler!!.layoutManager = LinearLayoutManager(context)
        contacts_recycler!!.adapter = adapter
        prefs = PreferenceHelper.defaultPrefs(context!!)

        activity!!.toolbar.setTitle(R.string.sharing_card)

        refresher?.setOnRefreshListener {
            getCards()
        }

        noCards?.setOnClickListener {
            val intent = Intent(context, CreateContactActivity::class.java)
            startActivity(intent)
        }

        getCards()

        return view
    }


    private fun getCards() {
        val stringRequest = object : JsonObjectRequest(
            Request.Method.GET,  Urls.getUrl(Urls.my_cards), null,
            Response.Listener { response ->
                activity!!.runOnUiThread {
                    refresher?.isRefreshing = false
                }
                try {
                    val gson = Gson()
                    val contactData: ContactQueryData = gson.fromJson(response.toString(),  ContactQueryData::class.java)
                    Log.d("Response",contactData.data.size.toString() +" my cards")
                    adapter.contactlist.clear()
                    adapter.contactlist.addAll(contactData.data)
                    activity!!.runOnUiThread {
                        adapter.notifyDataSetChanged()
                    }
                    if(adapter.contactlist.isEmpty()){
                        noCards!!.visibility = View.VISIBLE
                        contacts_recycler!!.visibility = View.INVISIBLE
                    }else{
                        noCards!!.visibility = View.INVISIBLE
                        contacts_recycler!!.visibility = View.VISIBLE
                        adapter.notifyDataSetChanged()
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }, Response.ErrorListener { error ->
                activity!!.runOnUiThread { refresher?.isRefreshing = false }
                val response = error.networkResponse
                if (response != null) {
                    try {
                        val res = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        Log.d("Response Error", res)
                        val obj = JSONObject(res)
                        val message = obj.getString("message")
                        context!!.showToast(message)
                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()
                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                    }
                }

            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return hashMapOf(
                    "Authorization" to "Bearer "+ prefs!!.getString(Constants.token, Constants.token),
                    "Content-Type" to "application/json"
                )
            }
        }
        VolleyService.requestQueue.add(stringRequest)
    }

}
