package com.webmastersfactory.cardbag.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.view.Menu
import android.view.MenuItem
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper.set
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.fragments.ContactsFragment
import com.webmastersfactory.cardbag.fragments.ScanFragment
import com.webmastersfactory.cardbag.fragments.ShareFragment
import com.webmastersfactory.cardbag.models.Urls
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private val contactsFragment: ContactsFragment = ContactsFragment()
    private val scanFragment: ScanFragment = ScanFragment()
    private val shareFragment: ShareFragment = ShareFragment()
    private var menuM: Menu? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out)

        when (item.itemId) {
            R.id.navigation_contacts -> transaction.replace(R.id.fragment_container,contactsFragment)
            R.id.navigation_add -> transaction.replace(R.id.fragment_container,scanFragment)
            R.id.navigation_share -> transaction.replace(R.id.fragment_container,shareFragment)
        }
        transaction.commit()
        true
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        if(!prefs!!.getBoolean(Constants.verified_email,true)){
            showToast("Verify email please")
            val intent = Intent(this,ConfirmEmailActivity::class.java)
            startActivity(intent)
            finish()
            return
        }

        if(!prefs!!.getBoolean(Constants.verified_phone,true)){
            showToast("Verify phone number please")
            val intent = Intent(this,ConfirmPhoneActivity::class.java)
            startActivity(intent)
            finish()
            return
        }

        if(!prefs!!.getBoolean(Constants.verified_phone,true)){
            showToast("Verify phone number please")
            val intent = Intent(this,ConfirmPhoneActivity::class.java)
            startActivity(intent)
            finish()
            return
        }

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragment_container,contactsFragment)
        transaction.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuM = menu
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.action_add_card -> {
                intent = Intent(this, CreateContactActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.action_profile -> {
                intent = Intent(this, ProfileActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.action_logout -> logOut()
        }

        return super.onOptionsItemSelected(item)
    }


    private fun logOut() {
        showProgressDialog()
        val stringRequest = object : JsonObjectRequest(
            Urls.getUrl(Urls.logout), null,
            Response.Listener {
                PreferenceHelper.emptyPrefs(this)
                runOnUiThread{
                    hideProgressDialog()
                    prefs!![Constants.token] = null
                    intent = Intent (this, LandingActivity::class.java )
                    finish()
                }
            },
            Response.ErrorListener {
                PreferenceHelper.emptyPrefs(this)
                runOnUiThread{
                    hideProgressDialog()
                    prefs!![Constants.token] = null
                    intent = Intent (this, LandingActivity::class.java )
                    finish()
                }
            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return hashMapOf(
                    "Authorization" to "Bearer "+ prefs!!.getString(Constants.token, Constants.token),
                    "Content-Type" to "application/json"
                )
            }
        }
        VolleyService.requestQueue.add(stringRequest)
    }
}
