package com.webmastersfactory.cardbag.activities

import android.os.Bundle
import com.webmastersfactory.cardbag.R
import com.google.zxing.WriterException
import android.graphics.Bitmap
import android.util.Log
import android.view.MenuItem
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.google.zxing.BarcodeFormat
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.isConnectedToNetwork
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.models.Urls
import kotlinx.android.synthetic.main.activity_qr.*
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset


class QRActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qr)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val token = intent.getIntExtra(Constants.contact_id,0)

        if(isConnectedToNetwork()){
            shareCard(token.toString())
        }else{
            showToast(getString(R.string.no_internet))
        }


    }

    private fun generateQr(tokenStr: String) {
        try {
            val qrCodeWriter = QRCodeWriter()
            val bitMatrix: BitMatrix = qrCodeWriter.encode(tokenStr,
            BarcodeFormat.QR_CODE,
            350, 350)

            val barcodeEncoder = BarcodeEncoder()
            val bitmap: Bitmap = barcodeEncoder.createBitmap(bitMatrix)

            //Assign value to our image view
            imgQr.setImageBitmap(bitmap)

        } catch (e: WriterException) {
            e.printStackTrace()
        }

    }

    private fun shareCard(contact_id:String) {
        showProgressDialog()
        val params = HashMap<String,String>()
        params.put("contact_id", contact_id)
        val stringRequest = object : JsonObjectRequest(
            Urls.getUrl(Urls.share_contact), JSONObject(params),
            Response.Listener { response ->
                try {
                    val obj = JSONObject(response.toString())
                    val tokenStr: String = obj.getString(Constants.token)
                    runOnUiThread{ hideProgressDialog()}
                    generateQr(tokenStr)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }, Response.ErrorListener { error ->
                val response = error.networkResponse
                if (response != null) {
                    try {
                        val res = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        Log.d("Response Error", res)
                        val obj = JSONObject(res)
                        val message = obj.getString("message")
                        showToast(message)
                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()
                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                    }
                }

            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return hashMapOf(
                    "Authorization" to "Bearer "+ prefs!!.getString(Constants.token, Constants.token),
                    "Content-Type" to "application/json"
                )
            }
        }
        VolleyService.requestQueue.add(stringRequest)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item!!.itemId == android.R.id.home){
            finish()
        }
        return true
    }

}
