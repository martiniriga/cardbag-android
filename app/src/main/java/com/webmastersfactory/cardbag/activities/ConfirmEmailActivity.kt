package com.webmastersfactory.cardbag.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper.set
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.models.Urls

import kotlinx.android.synthetic.main.activity_confirm_email.*
import kotlinx.android.synthetic.main.content_confirm_email.*
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.util.HashMap

class ConfirmEmailActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_email)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val emailStr =  prefs!!.getString(Constants.email,"")

        txtinstruction.text = String.format(getString(R.string.email_sent_with_code), emailStr)

        btnNext.setOnClickListener { confirmCode() }



    }

    private fun confirmCode() {
        if (!validate()) {
            return
        }
        showProgressDialog()

        val codeStr = emailCode!!.text.toString()

        val params = HashMap<String,String>()

        params.put("code", codeStr)


        val stringRequest = object : JsonObjectRequest(
            Urls.getUrl(Urls.register_email), JSONObject(params),
            Response.Listener {response->
                try {
                    val message = response.getString("message")
                    runOnUiThread {
                        hideProgressDialog()
                        showToast(message)
                        prefs!![Constants.email_valid] = Constants.email_valid
                        val intent = Intent(this, ConfirmPhoneActivity::class.java)
                        startActivity(intent)
                    }
                }catch (e: JSONException) {
                    e.printStackTrace()
                    runOnUiThread{
                        showToast("Failed to validate email code")
                    }
                }
            },
            Response.ErrorListener { error ->
                runOnUiThread{ hideProgressDialog() }
                val response = error.networkResponse
                if (response != null) {
                    try {
                        val res = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        Log.d(ConfirmEmailActivity.TAG, res)
                        val obj = JSONObject(res)
                        val errorObj = obj.getJSONObject("errors")
                        val message = errorObj.getJSONArray("code")
                        runOnUiThread{
                            showToast(message.join( " "))
                        }
                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()
                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                        runOnUiThread{
                            showToast("Failed to validate email code")
                        }
                    }
                }
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return hashMapOf(
                    "Authorization" to "Bearer "+ prefs!!.getString(Constants.token,Constants.token),
                    "Content-Type" to "application/json"
                )
            }

        }

        VolleyService.requestQueue.add(stringRequest)
    }

    fun validate(): Boolean {
        var valid = true
        var focusView: View? = null

        val codeStr = emailCode.text.toString()

        if (codeStr.isEmpty() || codeStr.length < 6) {
            emailCode.error = getString(R.string.error_code_short)
            focusView = emailCode
            valid = false
        } else {
            emailCode.error = null
        }

        if(!valid){
            focusView?.requestFocus()
        }

        return valid

    }


    companion object {
        private val TAG = ConfirmEmailActivity::class.simpleName
    }




}
