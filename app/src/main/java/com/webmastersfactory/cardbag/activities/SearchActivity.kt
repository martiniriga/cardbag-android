package com.webmastersfactory.cardbag.activities

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.adapters.ContactAdapter
import com.webmastersfactory.cardbag.models.ContactQueryData
import com.webmastersfactory.cardbag.models.Urls

import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.content_search.*
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset

class SearchActivity : BaseActivity() {
    var adapter: ContactAdapter = ContactAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        search_results_recycler.layoutManager = LinearLayoutManager(this)
        search_results_recycler.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item!!.itemId == android.R.id.home){
            finish()
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu,menu)

        val searchItem = menu!!.findItem(R.id.action_search)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = searchItem!!.actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setIconifiedByDefault(false)
        searchView.requestFocus()
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String): Boolean{
                searchContact(query)
                return false
            }

            override fun onQueryTextChange(s: String):Boolean{
                return false
            }
        })

        return true
    }

    private fun searchContact(query: String) {
        val stringRequest = object : JsonObjectRequest(
            Request.Method.GET,  Urls.getUrl(Urls.contacts_list+"?query="+query), null,
            Response.Listener { response ->
                try {
                    val gson = Gson()
                    val contactData: ContactQueryData = gson.fromJson(response.toString(),  ContactQueryData::class.java)
                    Log.d("Ati",contactData.data.size.toString() +"kadhaa")
                    adapter.contactlist.clear()
                    adapter.contactlist.addAll(contactData.data)
                    runOnUiThread {
                        adapter.notifyDataSetChanged()

                    }
                } catch (e: JSONException) {
                    showToast("Failed to fetch any results")
                    e.printStackTrace()
                }
            }, Response.ErrorListener { error ->
                Log.e(TAG, "Site Info Error: " + error.message)
                val response = error.networkResponse
                if (response != null) {
                    try {
                        val res = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        Log.d("Site Info Error2", res)
                        val obj = JSONObject(res)
                        val message = obj.getString("message")
                        showToast(message)
                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()
                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                    }
                }

            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return hashMapOf(
                    "Authorization" to "Bearer "+ prefs!!.getString(Constants.token, Constants.token),
                    "Content-Type" to "application/json"
                )
            }
        }

        VolleyService.requestQueue.add(stringRequest)
    }

    companion object {
        private val TAG = SearchActivity::class.simpleName
    }

}


