package com.webmastersfactory.cardbag.activities

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper.set
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.models.Urls

import kotlinx.android.synthetic.main.activity_confirm_phone.*
import kotlinx.android.synthetic.main.content_confirm_phone.*
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.util.*
import java.util.concurrent.TimeUnit


class ConfirmPhoneActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_phone)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btnNext.setOnClickListener { confirmPhone() }

        btnverify.setOnClickListener { getCode() }

    }


    private fun getCode() {

        if(!validatePhone()){
            return
        }

        showProgressDialog()

        val phoneStr = phone!!.text.toString()

        val params = HashMap<String,String>()

        params.put("phone", phoneStr)


        val stringRequest = object : JsonObjectRequest(
            Urls.getUrl(Urls.phone_code), JSONObject(params),
            Response.Listener { response ->
                var message: String? = null
                try {
                    message = response.getString("message")
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                timer(30000,1000).start()
                runOnUiThread{
                    hideProgressDialog()
                    showToast(message!!)
                    btnverify.text = getString(R.string.resend)
                    txtcode.visibility = View.VISIBLE
                    btnNext.visibility = View.VISIBLE
                    txtcode.requestFocus()
                }
            },
            Response.ErrorListener { error ->
                val response = error.networkResponse
                if (response != null) {
                    try {
                        runOnUiThread{
                            hideProgressDialog()
                            btnverify.text = getString(R.string.resend)
                        }
                        val res = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        Log.d(ConfirmPhoneActivity.TAG, res)
                        val obj = JSONObject(res)
                        val errorObj = obj.getJSONObject("errors")
                        val message = errorObj.getJSONArray("phone")
                        runOnUiThread{
                            txtcode.visibility = View.VISIBLE
                            btnNext.visibility = View.VISIBLE
                            showToast(message.join( " "))
                        }
                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()

                        runOnUiThread{ showToast("Failed to generate OTP code") }

                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                        runOnUiThread{ showToast("Failed to generate OTP code") }
                    }
                }
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return hashMapOf(
                    "Authorization" to "Bearer "+ prefs!!.getString(Constants.token,Constants.token),
                    "Content-Type" to "application/json"
                )
            }

        }

        VolleyService.requestQueue.add(stringRequest)
    }

    private fun confirmPhone() {
        if (!validate()) {
            return
        }

        showProgressDialog()

        val codeStr = txtcode!!.text.toString()

        val params = HashMap<String,String>()

        val phoneStr = phone!!.text.toString()

        params.put("code", codeStr)


        val stringRequest = object : JsonObjectRequest(
            Urls.getUrl(Urls.register_phone), JSONObject(params),
            Response.Listener {
                runOnUiThread{
                    hideProgressDialog()
                    showToast(getString(R.string.phone_verification_successful))
                    prefs!![Constants.phone] = phoneStr
                    val intent = Intent(this,CreateProfileActivity::class.java)
                    startActivity(intent)
                }

            },
            Response.ErrorListener { error ->
                runOnUiThread{ hideProgressDialog() }
                val response = error.networkResponse
                if (response != null) {
                    try {
                        val res = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        Log.d(ConfirmPhoneActivity.TAG, res)
                        val obj = JSONObject(res)
                        val errorObj = obj.getJSONObject("errors")
                        val message = errorObj.getJSONArray("code")
                        runOnUiThread{
                            showToast(message.join( " "))
                        }
                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()
                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                    }
                }
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return hashMapOf(
                    "Authorization" to "Bearer "+ prefs!!.getString(Constants.token,Constants.token),
                    "Content-Type" to "application/json"
                )
            }

        }

        VolleyService.requestQueue.add(stringRequest)
    }

    private fun validatePhone(): Boolean {
        var valid = true
        var focusView: View? = null

        val phoneStr = phone.text.toString()

        if (phoneStr.isEmpty() || phoneStr.length < 9) {
            phone.error = getString(R.string.phone)
            focusView = txtcode
            valid = false
        } else {
            phone.error = null
        }

        if(!valid){
            focusView?.requestFocus()
        }

        return valid

    }

    private fun validate(): Boolean {
        var valid = true
        var focusView: View? = null

        val codeStr = txtcode.text.toString()

        if (codeStr.isEmpty() || codeStr.length < 6) {
            txtcode.error = getString(R.string.error_code_short)
            focusView = txtcode
            valid = false
        } else {
            txtcode.error = null
        }

        if(!valid){
            focusView?.requestFocus()
        }

        return valid

    }

    private fun timer(millisInFuture:Long,countDownInterval:Long): CountDownTimer {
        textTimer.visibility =  View.VISIBLE
        btnverify.visibility =  View.INVISIBLE
        return object: CountDownTimer(millisInFuture,countDownInterval){
            override fun onTick(millisUntilFinished: Long){
                val timeRemaining = timeString(millisUntilFinished)
                textTimer.text = timeRemaining

            }

            override fun onFinish() {
                    textTimer.visibility = View.INVISIBLE
                    btnverify.visibility = View.VISIBLE

            }
        }
    }

    private fun timeString(millisUntilFinished:Long):String{

        val seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)

        // Format the string
        return String.format(
            Locale.getDefault(),
            "%02d seconds until you can retry sending an sms",
            seconds
        )
    }



    companion object {
        private val TAG = ConfirmPhoneActivity::class.simpleName
    }


}
