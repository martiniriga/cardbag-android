package com.webmastersfactory.cardbag.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.isConnectedToNetwork
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.models.Contact
import com.webmastersfactory.cardbag.models.Urls
import kotlinx.android.synthetic.main.activity_edit_contact.*
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset

class EditContactActivity : BaseActivity() {

    private var contactStr:String? = "Personal"
    private var contact: Contact? = null
    private var adapter: ArrayAdapter<String>? = null
    private var contactJson:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_contact)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        populateContact()

        updateAdapter()
    }

    private fun updateAdapter() {

        adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, listOf("Business", "Personal"))
        adapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        contact_type.adapter = adapter

        contact_type.setSelection(adapter?.getPosition(contact!!.type!!)!!)

        contact_type.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                contactStr = adapter?.getItem(position)
                if(contactStr == "Business"){
                    linToggle.visibility = View.VISIBLE
                }else{
                    linToggle.visibility = View.GONE
                }
            }
        }

    }

    private fun populateContact() {

        contactJson = intent.getStringExtra(Constants.contact)

        contact = Gson().fromJson<Contact>(contactJson, Contact::class.java)

        email.setText(contact!!.email)
        phone.setText(contact!!.phone)
        company.setText(contact!!.company)
        occupation.setText(contact!!.occupation)
        physical_address.setText(contact!!.physical_address)
        postal_address.setText(contact!!.postal_address)
        website.setText(contact!!.website)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_contact_edit, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_save -> {
                if(isConnectedToNetwork()){
                    saveContact()
                }else{
                    showToast(getString(R.string.no_internet))
                }

                true
            }
            android.R.id.home -> {
                intent = Intent(this,ContactDetailActivity::class.java)
                intent.putExtra(Constants.contact,contactJson)
                intent.putExtra(Constants.is_owner,true)
                startActivity(intent)
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun saveContact() {

        if (!validate()) {
            return
        }

        showProgressDialog()

        val emailStr = email!!.text.toString()
        val phoneStr = phone!!.text.toString()
        val companyStr = company!!.text.toString()
        val occupationStr = occupation!!.text.toString()
        val physicaladdressStr = physical_address!!.text.toString()
        val postaladdressStr = postal_address!!.text.toString()
        val websiteStr = website!!.text.toString()


        val params = HashMap<String,String>()
        params.put("email", emailStr)
        params.put("phone", phoneStr )
        params.put("company", companyStr)
        params.put("occupation", occupationStr)
        params.put("physical_address", physicaladdressStr )
        params.put("postal_address", postaladdressStr )
        params.put("website", websiteStr)
        params.put("contact_type", contactStr!!)


        val stringRequest = object : JsonObjectRequest(
            Urls.getUrl(Urls.contacts + "/" + contact!!.id), JSONObject(params),
            Response.Listener {
                runOnUiThread{
                    hideProgressDialog()
                    try {
                        showToast(getString(R.string.contact_updated))
                        goToContacts()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            },
            Response.ErrorListener { error ->
                runOnUiThread{hideProgressDialog()}
                val response = error.networkResponse
                if (response != null) {
                    try {
                        val res = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        Log.d("Response error", res)
                        val obj = JSONObject(res)
                        val message = obj.getString("message")
                        runOnUiThread{
                            showToast(message)
                        }
                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()
                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                    }
                }
            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return hashMapOf(
                "Authorization" to "Bearer "+ prefs!!.getString(Constants.token,Constants.token),
                "Content-Type" to "application/json"
                )
            }

        }

        VolleyService.requestQueue.add(stringRequest)

    }


    private fun validate(): Boolean {
        var valid = true
        var focusView: View? = null

        val emailStr = email.text.toString()
        val phoneStr = phone.text.toString()

        if(contactStr.isNullOrEmpty()){
            showToast("Please select contact type")
            focusView = contact_type
            valid = false
        }

        if (phoneStr.isEmpty() || phoneStr.length < 10) {
            phone.error = "enter a valid phone"
            focusView = phone
            valid = false
        } else {
            phone.error = null
        }

        if (!emailStr.isEmpty() && !Constants.isValidEmail(emailStr)) {
            email.error = getString(R.string.error_invalid_email)
            focusView = email
            valid = false
        } else {
            email!!.error = null
        }

        if(!valid){
            focusView?.requestFocus()
        }

        return valid
    }



}
