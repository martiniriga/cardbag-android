package com.webmastersfactory.cardbag.activities

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.Utilities.isConnectedToNetwork
import com.webmastersfactory.cardbag.models.Contact
import com.webmastersfactory.cardbag.models.Urls
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_contact_detail.*
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset




class ContactDetailActivity : BaseActivity() {

    private var contact:Contact? = null
    private var isOwner = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_contact_detail)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        populateContact()

    }

    private fun populateContact() {

        isOwner = intent.getBooleanExtra(Constants.is_owner,false)

        invalidateOptionsMenu()

        val contactJson = intent.getStringExtra(Constants.contact)

        contact = Gson().fromJson<Contact>(contactJson,Contact::class.java)

        if(contact!!.type == "Business"){
            linToggle.visibility = View.VISIBLE
        }else{
            linToggle.visibility = View.GONE
        }

        supportActionBar?.title = contact!!.names

        call.setOnClickListener{
            makeCall()
        }

        email.setOnClickListener{
            val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", contact!!.email, null))
            startActivity(intent)
        }

        names.text = contact!!.names
        occupation.text = contact!!.occupation
        txtemail.text = contact!!.email
        txtpostaladdress.text = contact!!.postal_address
        txtphysicaladdress.text = contact!!.physical_address
        txtcompany.text = contact!!.company
        txtphone.text = contact!!.phone

        val webStr = contact!!.website

        txtwebsite.text = webStr

        val photoStr = contact!!.photo_url

        if(!photoStr.isNullOrEmpty()){
            Picasso.get()
                .load(photoStr)
                .placeholder(R.drawable.ic_account_box)
                .resize(120,120)
                .into(contact_image)
        }

        if(!webStr.isNullOrEmpty()){
            browser.setOnClickListener{openWebPage(webStr!!)}
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        menuInflater.inflate(R.menu.menu_contact_detail, menu)
        return true
    }

    fun openWebPage(url: String) {
        val webpage: Uri = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, webpage)
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) :Boolean {
        val item = menu.findItem(R.id.action_edit)
        item.isVisible = isOwner

        val callitem = menu.findItem(R.id.action_call)
        callitem.isVisible = !isOwner

        val smsitem = menu.findItem(R.id.action_send_sms)
        smsitem.isVisible = !isOwner

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item!!.itemId
        when (id) {
            R.id.home -> {
                finish()
                return true
            }
            R.id.action_call -> {
                makeCall()
                return true
            }
            R.id.action_send_sms -> {
                sendSMS()
                return true
            }
            R.id.action_delete -> {
                deleteContact()
                return true
            }
            R.id.action_edit -> {
                intent = Intent(this,EditContactActivity::class.java)
                val contactJson = Gson().toJson(contact,Contact::class.java)
                intent.putExtra(Constants.contact,contactJson)
                startActivity(intent)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun sendSMS() {
        val uri = Uri.parse("smsto:${contact!!.phone}")
        val it = Intent(Intent.ACTION_SENDTO, uri)
        startActivity(it)
    }

    private fun makeCall() {
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", contact!!.phone, null))
        startActivity(intent)

    }


    private fun deleteContact() {

        val builder = AlertDialog.Builder(this)

        builder.setTitle(getString(R.string.delete))
            .setMessage(getString(R.string.confirm_delete))
            .setCancelable(true)
            .setPositiveButton("YES"){ _, _ ->

                if(isConnectedToNetwork()){

                    removeContact()

                }else{

                    showToast(getString(R.string.no_internet))
                }

            }
            .setNegativeButton(R.string.no) {_,_ ->

            }

        val dialog: AlertDialog = builder.create()

        dialog.show()

    }

    private fun removeContact() {

        showProgressDialog()

        val stringRequest = object : JsonObjectRequest(

            Request.Method.POST, Urls.getUrl(Urls.delete_contact + contact!!.id), null,
            Response.Listener {

                    runOnUiThread {
                        hideProgressDialog()
                        goToContacts()
                    }

            }, Response.ErrorListener { error ->
                val response = error.networkResponse

                if (response != null) {

                    try {
                        val res = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        Log.d("Response Error", res)

                        runOnUiThread {

                            hideProgressDialog()

                            showToast(getString(R.string.failed_to_delete))
                        }

                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()
                    }
                }

            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return hashMapOf(
                    "Authorization" to "Bearer "+ prefs!!.getString(Constants.token, Constants.token),
                    "Content-Type" to "application/json"
                )
            }
        }
        VolleyService.requestQueue.add(stringRequest)
    }


}
