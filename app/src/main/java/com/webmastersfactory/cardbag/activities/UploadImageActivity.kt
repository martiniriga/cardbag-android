package com.webmastersfactory.cardbag.activities


import android.os.Bundle
import com.webmastersfactory.cardbag.R

import kotlinx.android.synthetic.main.activity_upload_image.*
import kotlinx.android.synthetic.main.content_upload_image.*

import android.content.Intent

import android.net.Uri

import com.imagepicker.LifeCycleCallBackManager
import com.imagepicker.FilePickUtils

import com.imagepicker.FilePickUtils.CAMERA_PERMISSION
import com.imagepicker.FilePickUtils.STORAGE_PERMISSION_IMAGE
import android.util.Log
import android.view.View
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper.set
import com.webmastersfactory.cardbag.Utilities.isConnectedToNetwork
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.models.Urls
import java.io.*
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject


class UploadImageActivity : BaseActivity() {

    private var mCropImageUri: Uri? = null

    private var filePickUtils: FilePickUtils? = null

    private var lifeCycleCallBackManager: LifeCycleCallBackManager? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_image)
        setSupportActionBar(toolbar)

        filePickUtils = FilePickUtils(this, onFileChoose)
        lifeCycleCallBackManager = filePickUtils!!.callBackManager

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btnNext.setOnClickListener {
            if(isConnectedToNetwork()) {

                showProgressDialog()

                Thread {
                    uploadImage()
                }.start()

            }else{
                showToast(getString(R.string.no_internet))
            }
        }

        imgFolder.setOnClickListener {
            filePickUtils!!.requestImageGallery(STORAGE_PERMISSION_IMAGE, true, true)
        }


        imgCamera.setOnClickListener{
            filePickUtils!!.requestImageCamera(CAMERA_PERMISSION, true, true)
        }

    }



    private fun uploadImage() {

        val client = OkHttpClient()

        val file = File(mCropImageUri!!.path)

        Log.i(TAG,"uri: "+mCropImageUri!!.path)

        val MEDIA_TYPE_JPEG : MediaType? = MediaType.parse("image/jpeg")

        val requestBody : RequestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("photo", "photo.jpg", RequestBody.create(MEDIA_TYPE_JPEG, file))
            .build()

        var request : Request = Request.Builder()
            .url(Urls.getUrl(Urls.upload_photo))
            .header("Accept", "application/json")
            .header("Content-Type", "multipart/form-data")
            .header("Authorization","Bearer "+ prefs!!.getString(Constants.token, Constants.token))
            .post(requestBody).build()

        try {
            var response  = client.newCall(request).execute()
            if (!response.isSuccessful){
                val res = response.body()!!.string()
                Log.d(TAG, res)
                runOnUiThread{
                    hideProgressDialog()
                    showToast("Failed to upload")
                }

            }else {
                try {
                    val res = response.body()!!.string()
                    response.close()
                    Log.d(TAG, res)
                    val obj = JSONObject(res)
                    prefs!![Constants.photo_url] = obj.getString(Constants.photo_url)
                    val message = obj.getString("message")
                    runOnUiThread{
                        hideProgressDialog()
                        showToast(message)
                        goToContacts()
                        finish()
                    }
                } catch (e1: UnsupportedEncodingException) {
                    e1.printStackTrace()
                } catch (e2: JSONException) {
                    e2.printStackTrace()
                }
            }
        }catch (e: IOException){
            Log.d(TAG, e.message)
        }

    }


    private val onFileChoose = FilePickUtils.OnFileChoose { fileUri, _, _->
        // here you will get captured or selected image<br>
        mCropImageUri = Uri.fromFile( File(fileUri))
        imgProfile.setImageURI(mCropImageUri)
        btnNext.visibility = View.VISIBLE
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (lifeCycleCallBackManager != null) {
            lifeCycleCallBackManager!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (lifeCycleCallBackManager != null) {
            lifeCycleCallBackManager!!.onActivityResult(requestCode, resultCode, data)
        }
    }


    companion object {
        private val TAG = UploadImageActivity::class.simpleName
    }

}
