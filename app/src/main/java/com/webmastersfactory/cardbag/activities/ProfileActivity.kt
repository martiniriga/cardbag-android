package com.webmastersfactory.cardbag.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.imagepicker.FilePickUtils
import com.imagepicker.LifeCycleCallBackManager
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper.get
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper.set
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.isConnectedToNetwork
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.models.Urls
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_profile.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset

class ProfileActivity : BaseActivity() {

    private var mCropImageUri: Uri? = null
    private var filePickUtils: FilePickUtils? = null
    private var lifeCycleCallBackManager: LifeCycleCallBackManager? = null

    private var photoUrl: String? = null
    private var nameStr: String? = null
    private var emailStr: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        filePickUtils = FilePickUtils(this, onFileChoose)
        lifeCycleCallBackManager = filePickUtils!!.callBackManager

        imgFolder.setOnClickListener {
            filePickUtils!!.requestImageGallery(FilePickUtils.STORAGE_PERMISSION_IMAGE, true, true)
        }


        imgCamera.setOnClickListener{
            filePickUtils!!.requestImageCamera(FilePickUtils.CAMERA_PERMISSION, true, true)
        }

        getProfile()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.home -> {
                finish()
                true
            }
            R.id.action_save -> {

                if(isConnectedToNetwork()){
                    updateProfile()
                }else{
                    showToast(getString(R.string.no_internet))
                }

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_contact_create, menu)
        return true
    }


    private fun updateProfile() {

        val focusView: View?
        if (txtname!!.text.isEmpty()) {
            txtname.error = "Enter name"
            focusView = txtname
            focusView?.requestFocus()
            return
        }

        showProgressDialog()

        val params = HashMap<String,String>()

        params.put("name", txtname!!.text.toString())

        val stringRequest = object : JsonObjectRequest(
            Urls.getUrl(Urls.profile), JSONObject(params),
            Response.Listener { response ->

                    try {
                        runOnUiThread {
                            hideProgressDialog()
                            val message = response.getString("message")
                            showToast(message)
                        }
                    } catch (e: JSONException) {
                        Log.d(TAG,"Response error $e" )
                        e.printStackTrace()
                    }
            },
            Response.ErrorListener { error ->
                val response = error.networkResponse
                if (response != null) {
                    try {
                        val res = String(response.data,
                            Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        val obj = JSONObject(res)
                        val err = obj.getJSONObject("error")
                        val messageArr = err.getJSONArray("message")
                        var message = ""
                        for (i in 0 until messageArr.length()) {
                            message += messageArr.get(i).toString() + "\n"
                        }
                        Log.d(TAG,"Response error $message")
                        runOnUiThread{ showToast( message, Toast.LENGTH_LONG)}

                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()
                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                    }

                }
            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return hashMapOf(
                    "Authorization" to "Bearer "+ prefs!![Constants.token],
                    "Content-Type" to "application/json"
                )
            }

        }

        VolleyService.requestQueue.add(stringRequest)
        VolleyService.requestQueue.start()
    }


    private fun getProfile() {

        photoUrl  = prefs!![Constants.photo_url]
        nameStr = prefs!![Constants.name]
        emailStr = prefs!![Constants.name]

        txtname.setText(nameStr)
        txtemail.setText(emailStr)

        if (photoUrl != null) {
            Picasso.get()
                .load(photoUrl)
                .placeholder(R.drawable.ic_account_box)
                .resize(150, 140)
                .centerCrop()
                .into(imgProfile)
        }

    }

    private fun uploadImage() {

        val client = OkHttpClient()

        val file = File(mCropImageUri!!.path)

        Log.i(TAG,"uri: "+mCropImageUri!!.path)

        val MEDIA_TYPE_JPEG : MediaType? = MediaType.parse("image/jpeg")

        val requestBody : RequestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("photo", "photo.jpg", RequestBody.create(MEDIA_TYPE_JPEG, file))
            .build()

        var request : okhttp3.Request = okhttp3.Request.Builder()
            .url(Urls.getUrl(Urls.upload_photo))
            .header("Accept", "application/json")
            .header("Content-Type", "multipart/form-data")
            .header("Authorization","Bearer "+ prefs!!.getString(Constants.token, Constants.token))
            .post(requestBody).build()

        try {
            var response  = client.newCall(request).execute()
            if (!response.isSuccessful){
                val res = response.body()!!.string()
                Log.d(TAG, res)
                runOnUiThread{
                    hideProgressDialog()
                    showToast("Failed to upload")
                }

            }else {
                try {
                    val res = response.body()!!.string()
                    response.close()
                    Log.d(TAG, res)
                    val obj = JSONObject(res)
                    prefs!![Constants.photo_url] = obj.getString(Constants.photo_url)
                    val message = obj.getString("message")
                    runOnUiThread{
                        hideProgressDialog()
                        showToast(message)
                    }
                } catch (e1: UnsupportedEncodingException) {
                    e1.printStackTrace()
                } catch (e2: JSONException) {
                    e2.printStackTrace()
                }
            }
        }catch (e: IOException){
            Log.d(TAG, e.message)
        }

    }

    private val onFileChoose = FilePickUtils.OnFileChoose { fileUri, _, _->
        imgProfile.setImageURI(Uri.fromFile( File(fileUri)))
        mCropImageUri = Uri.fromFile( File(fileUri))
        runOnUiThread{ showProgressDialog()}
        Thread {
            uploadImage()
        }.start()
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (lifeCycleCallBackManager != null) {
            lifeCycleCallBackManager!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (lifeCycleCallBackManager != null) {
            lifeCycleCallBackManager!!.onActivityResult(requestCode, resultCode, data)
        }
    }

    companion object {
        private val TAG = ProfileActivity::class.simpleName
    }

}
