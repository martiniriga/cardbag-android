package com.webmastersfactory.cardbag.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.Gson
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper.set
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.models.ProfileQueryData
import com.webmastersfactory.cardbag.models.Urls
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.util.*


class RegisterActivity : BaseActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        registerButton.setOnClickListener{ signUp()}

    }



    private fun signUp() {
        if (!validate()) {
            onRegisterFailed()
            return
        }

        hideKeyboard(registerView)

        showProgressDialog()

        val emailStr = email!!.text.toString()
        val passwordStr = password!!.text.toString()

        val params = HashMap<String,String>()

        params.put("email", emailStr)
        params.put("password", passwordStr)
        params.put("signup", "Android")


        val stringRequest = object : JsonObjectRequest(
            Urls.getUrl(Urls.register), JSONObject(params),
            Response.Listener { response ->
                try {
                    Log.i(TAG,"Response: " +response.toString())
                    val gson = Gson()
                    val profileData: ProfileQueryData = gson.fromJson(response.toString(),  ProfileQueryData::class.java)
                    prefs!![Constants.token] = profileData.data!!.token
                    prefs!![Constants.email] = emailStr
                    prefs!![Constants.phone] = profileData.data.phone
                    prefs!![Constants.photo_url] = profileData.data.photo_url
                    prefs!![Constants.verified_email] = profileData.data.verified_email
                    prefs!![Constants.verified_phone] = profileData.data.verified_phone
                    prefs!![Constants.name] = profileData.data.name
                    runOnUiThread {
                        hideProgressDialog()
                        when {
                            profileData.data.name != "" -> {
                                goToContacts()
                                finish()
                            }
                            profileData.data.verified_phone -> {
                                intent = Intent(this, CreateProfileActivity::class.java)
                                startActivity(intent)
                                finish()
                            }
                            profileData.data.verified_email -> {
                                intent = Intent(this, ConfirmPhoneActivity::class.java)
                                startActivity(intent)
                                finish()
                            }
                            else -> {
                                intent = Intent(this, ConfirmEmailActivity::class.java)
                                startActivity(intent)
                                finish()
                            }
                        }

                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            },
            Response.ErrorListener { error ->
                runOnUiThread {
                    hideProgressDialog()
                    showToast(error.toString())
                }
                val response = error.networkResponse
                if (response != null) {
                    try {
                        val res = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        Log.d(TAG,"Response error "+ res)
//                        val obj = JSONObject(res)
//                        val message = obj.getString("message")
//                        runOnUiThread{showToast(message)}
                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()
                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                    }
                }
                Log.i(TAG, "Response error " +error.toString())
            }) {

        }

        VolleyService.requestQueue.add(stringRequest)
        VolleyService.requestQueue.start()
    }

    fun onRegisterFailed() {
        showToast( "Sign Up failed", Toast.LENGTH_LONG)
    }


    fun validate(): Boolean {
        var valid = true
        var focusView: View? = null

        val emailStr = email.text.toString()
        val passwordStr = password.text.toString()



        if (passwordStr.isEmpty() || passwordStr.length < 6) {
            password.error = getString(R.string.error_invalid_password)
            focusView = password
            valid = false
        } else {
            password.error = null
        }


        if (emailStr.isEmpty() || ! Constants.isValidEmail(emailStr)) {
            email.error = getString(R.string.error_invalid_email)
            focusView = email
            valid = false
        } else {
            email.error = null
        }



        if(!valid){
            focusView?.requestFocus()
        }

        return valid
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item!!.itemId == android.R.id.home){
            finish()
        }
        return true
    }

    companion object {
        private val TAG = RegisterActivity::class.simpleName
    }


}
