package com.webmastersfactory.cardbag.activities

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.isConnectedToNetwork
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.models.Urls
import kotlinx.android.synthetic.main.activity_create_contact.*
import kotlinx.android.synthetic.main.content_create_contact.*
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset

class CreateContactActivity : BaseActivity() {

    var contactStr:String? = "Personal"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_contact)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, listOf("Business", "Personal"))
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        contact_type.adapter = adapter

        contact_type.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                contactStr = adapter.getItem(position)
                if(contactStr == "Business"){
                    linToggle.visibility = View.VISIBLE
                }else{
                    linToggle.visibility = View.GONE
                }
            }
        }
    }



    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_contact_create, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_save -> {

                if(isConnectedToNetwork()){
                    saveContact()
                }else{
                    showToast(getString(R.string.no_internet))
                }

                true
            }
            R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun saveContact() {

//        if (!validate()) {
//            return
//        }

        showProgressDialog()

        val emailStr = email!!.text.toString()
        val phoneStr = phone!!.text.toString()
        val companyStr = company!!.text.toString()
        val occupationStr = occupation!!.text.toString()
        val physicaladdressStr = physical_address!!.text.toString()
        val postaladdressStr = postal_address!!.text.toString()
        val websiteStr = website!!.text.toString()


        val params = HashMap<String,String>()
        params.put("email", emailStr)
        params.put("phone", phoneStr )
        params.put("company", companyStr)
        params.put("occupation", occupationStr)
        params.put("physical_address", physicaladdressStr )
        params.put("postal_address", postaladdressStr )
        params.put("website", websiteStr)
        params.put("contact_type", contactStr!!)


        val stringRequest = object : JsonObjectRequest(
            Urls.getUrl(Urls.contacts), JSONObject(params),
            Response.Listener {
                runOnUiThread{
                    hideProgressDialog()
                    showToast("Contact created successfully")
                    goToContacts()
                }
            },
            Response.ErrorListener { error ->
                runOnUiThread{hideProgressDialog()}
                val response = error.networkResponse
                if (response != null) {
                    try {
                        val res = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        Log.d("Response error", res)
                        val obj = JSONObject(res)
                        val errorobj = obj.getJSONObject("errors")
                        var msgString =""
                        for (i in 0..(errorobj.length() - 1)) {
                            msgString += errorobj.getJSONArray(errorobj.names()[i].toString()).join(",")
                        }
                        runOnUiThread{
                            showToast(msgString,Toast.LENGTH_LONG)
                        }
                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()
                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                    }
                }
            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return hashMapOf(
                "Authorization" to "Bearer "+ prefs!!.getString(Constants.token,Constants.token),
                "Content-Type" to "application/json"
                )
            }

        }

        VolleyService.requestQueue.add(stringRequest)

    }


    private fun validate(): Boolean {
        var valid = true
        var focusView: View? = null

        val emailStr = email.text.toString()
        val phoneStr = phone.text.toString()

        if(contactStr.isNullOrEmpty()){
            showToast("Please select contact type")
            focusView = contact_type
            valid = false
        }

        if (phoneStr.isEmpty() || phoneStr.length < 10) {
            phone.error = "enter a valid phone"
            focusView = phone
            valid = false
        } else {
            phone.error = null
        }

        if (!emailStr.isEmpty() && !Constants.isValidEmail(emailStr)) {
            email.error = getString(R.string.error_invalid_email)
            focusView = email
            valid = false
        } else {
            email!!.error = null
        }

        if(!valid){
            focusView?.requestFocus()
        }

        return valid
    }



}
