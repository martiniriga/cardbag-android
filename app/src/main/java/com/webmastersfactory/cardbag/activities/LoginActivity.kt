package com.webmastersfactory.cardbag.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper.set
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.models.Urls
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import org.json.JSONObject
import com.android.volley.toolbox.HttpHeaderParser
import com.google.gson.Gson
import com.webmastersfactory.cardbag.Utilities.isConnectedToNetwork
import com.webmastersfactory.cardbag.models.ProfileQueryData
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset



class LoginActivity : BaseActivity(){


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        signInButton.setOnClickListener{
            if(isConnectedToNetwork()) {
                signIn()
            }else{
                showToast(getString(R.string.no_internet))
            }
        }
        googlesignInButton.setOnClickListener{
            if(isConnectedToNetwork()) {
                googleSignIn()
            }else{
                    showToast(getString(R.string.no_internet))
                }
            }
    }



    private fun signIn() {

        if (!validate()) {
            onLoginFailed()
            return
        }
        hideKeyboard(loginView)

        showProgressDialog()

        val txtemail = email!!.text.toString()
        val txtpassword = password!!.text.toString()


        val params = HashMap<String,String>()
        params.put("email", txtemail)
        params.put("password",txtpassword )


        val stringRequest = object : JsonObjectRequest(
            Urls.getUrl(Urls.login), JSONObject(params),
            Response.Listener { response ->
                try {
                    val gson = Gson()
                    val profileData: ProfileQueryData = gson.fromJson(response.toString(),  ProfileQueryData::class.java)
                    Log.d(TAG,"Response $response")
                    runOnUiThread{
                        hideProgressDialog()
                        try {
                            signInButton!!.isEnabled = true
                            prefs!![Constants.token] = profileData.data!!.token
                            prefs!![Constants.email] = txtemail
                            prefs!![Constants.phone] = profileData.data.phone
                            prefs!![Constants.photo_url] = profileData.data.photo_url
                            prefs!![Constants.verified_email] = profileData.data.verified_email
                            prefs!![Constants.verified_phone] = profileData.data.verified_phone
                            if(profileData.data.verified_phone) {
                                goToContacts()
                                finish()
                            }else{
                                if(profileData.data.verified_email) {
                                    intent = Intent(this@LoginActivity, ConfirmPhoneActivity::class.java)
                                    startActivity(intent)
                                }else{
                                    intent = Intent(this@LoginActivity, ConfirmEmailActivity::class.java)
                                    startActivity(intent)
                                }

                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }

                } catch (e2: JSONException) {
                    e2.printStackTrace()
                }

            },
            Response.ErrorListener { error ->
                runOnUiThread{
                    hideProgressDialog()
                    signInButton!!.isEnabled = true
                }
                val response = error.networkResponse
                if (response != null) {
                    try {
                        val res = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        Log.d(TAG,"Response $res")
                        val obj = JSONObject(res)
                        val message = obj.getString("error")
                        runOnUiThread{showToast(message)}
                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()
                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                    }
                }
            }) {

        }
        VolleyService.requestQueue.add(stringRequest)
    }


    private fun googleSignIn() {
        val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    private fun onLoginFailed() {
        showToast( "Login failed", Toast.LENGTH_LONG)
    }


    private fun validate(): Boolean {
        var valid = true
        var focusView: View? = null

        val emailStr = email.text.toString()
        val passwordStr = password.text.toString()

        if (passwordStr.isEmpty() || passwordStr.length < 6) {
            password.error = getString(R.string.error_invalid_password)
            focusView = password
            valid = false
        } else {
            password.error = null
        }

        if (!Constants.isValidEmail(emailStr)) {
            email.error = getString(R.string.error_invalid_email)
            focusView = email
            valid = false
        } else {
            email!!.error = null
        }

       if(!valid){
           focusView?.requestFocus()
       }

        return valid
    }



    companion object {
        private val TAG = LoginActivity::class.simpleName
    }


}
