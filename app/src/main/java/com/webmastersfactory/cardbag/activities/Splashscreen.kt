package com.webmastersfactory.cardbag.activities

import android.content.Intent
import android.os.Bundle
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import kotlin.concurrent.thread

class Splashscreen : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)
    }

    override fun onResume() {
        super.onResume()
        val nameString = prefs!!.getString(Constants.name, null)
        thread(start = true){
            Thread.sleep(3000L)
            if(nameString!= null){
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }else{
                val intent = Intent(this, LandingActivity::class.java)
                startActivity(intent)
                finish()
            }
        }

    }
}
