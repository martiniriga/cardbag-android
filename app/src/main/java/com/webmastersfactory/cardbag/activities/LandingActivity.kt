package com.webmastersfactory.cardbag.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper.set
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.models.Urls
import kotlinx.android.synthetic.main.activity_landing.*
import org.json.JSONException
import org.json.JSONObject
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import com.google.gson.Gson
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper.get
import com.webmastersfactory.cardbag.models.ProfileQueryData


class LandingActivity : BaseActivity() {


    private lateinit var auth: FirebaseAuth
    // [END declare_auth]

    private lateinit var googleSignInClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
        setContentView(R.layout.activity_landing)

        signUpButton.setOnClickListener{ signUp() }
        signInButton.setOnClickListener{ login() }
        googlesignUpButton.setOnClickListener { googleSignUp() }

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)

    }

    private fun googleSignUp() {
        try {

            val account = GoogleSignIn.getLastSignedInAccount(this)

            if (account != null) {

                sendToken(account.idToken!!)

            }else{

                val signInIntent = googleSignInClient.getSignInIntent()

                startActivityForResult(signInIntent, RC_SIGN_IN)
            }

        } catch (e: Exception) {
            Log.w(TAG, "signInResult:failed code=" + e.message)
            showToast("Google sign in failed")
        }
    }

    private fun signUp() {
        val intent = Intent(this@LandingActivity, RegisterActivity::class.java)
        startActivity(intent)
    }

    private fun login() {
        val intent = Intent(this@LandingActivity, LoginActivity::class.java)
        startActivity(intent)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)

        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            if (account != null) {
                showProgressDialog()
                sendToken(account.idToken!!)
            }
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.statusCode)
            showToast("Google sign in failed")
        }

    }


    public override fun onStart() {
        super.onStart()

        val nameStr: String? = prefs!![Constants.name]

        if( nameStr!= null){
            goToContacts()
        }
    }

    private fun sendToken(token: String) {

        val params = HashMap<String,String>()
        params.put("google_token", token)

        Log.d(TAG,"Response google_token $token")

        val stringRequest = object : JsonObjectRequest(
            Urls.getUrl(Urls.auth_google), JSONObject(params),
            Response.Listener { response ->
                runOnUiThread { hideProgressDialog() }
                try {
                    val gson = Gson()
                    Log.d(TAG,"Response $response")
                    val profileData: ProfileQueryData = gson.fromJson(response.toString(),  ProfileQueryData::class.java)
                    runOnUiThread {
                        hideProgressDialog()
                        try {
                            signInButton!!.isEnabled = true
                            prefs!![Constants.token] = profileData.data!!.token
                            prefs!![Constants.email] = profileData.data.email
                            prefs!![Constants.phone] = profileData.data.phone
                            prefs!![Constants.photo_url] = profileData.data.photo_url
                            prefs!![Constants.verified_email] = profileData.data.verified_email
                            prefs!![Constants.verified_phone] = profileData.data.verified_phone
                            if (!profileData.data.name.isNullOrEmpty()) {
                                goToContacts()
                            } else if (!profileData.data.verified_email) {
                                intent = Intent(this, ConfirmEmailActivity::class.java)
                                startActivity(intent)
                                finish()
                            } else if (!profileData.data.verified_phone) {
                                    intent = Intent(this, ConfirmPhoneActivity::class.java)
                                    startActivity(intent)
                                    finish()
                            } else {
                                intent = Intent(this, CreateProfileActivity::class.java)
                                startActivity(intent)
                                finish()
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                } catch (e2: JSONException) {
                    e2.printStackTrace()
                }
            },
            Response.ErrorListener { error ->
                runOnUiThread {
                    hideProgressDialog()
                    showToast("Google sign in failed")
                }
                Log.i(TAG,error.toString())
            }) {

        }

        VolleyService.requestQueue.add(stringRequest)
    }

    companion object {
        private val TAG = LoginActivity::class.simpleName
        private const val RC_SIGN_IN = 9001
    }
}
