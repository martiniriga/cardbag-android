package com.webmastersfactory.cardbag.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.webmastersfactory.cardbag.R
import com.webmastersfactory.cardbag.Utilities.Constants
import com.webmastersfactory.cardbag.Utilities.VolleyService
import com.webmastersfactory.cardbag.Utilities.showToast
import com.webmastersfactory.cardbag.models.Urls
import com.webmastersfactory.cardbag.Utilities.PreferenceHelper.set
import com.webmastersfactory.cardbag.Utilities.isConnectedToNetwork

import kotlinx.android.synthetic.main.activity_create_profile.*
import kotlinx.android.synthetic.main.content_create_profile.*
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset

class CreateProfileActivity : BaseActivity() {

    private var contactStr:String? = "Personal"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_profile)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        linToggle.visibility = View.GONE

        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, listOf("Business", "Personal"))
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        contact_type.adapter = adapter

        contact_type.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                contactStr = adapter.getItem(position)
                if(contactStr == "Business"){
                    linToggle.visibility = View.VISIBLE
                }else{
                    linToggle.visibility = View.GONE
                }
            }
        }

        btnNext.setOnClickListener {
            if(isConnectedToNetwork()) {
                submitProfile()
            }else{
                showToast(getString(R.string.no_internet))
            }
        }
    }

    private fun submitProfile() {
        if (!validate()) {
            return
        }

        showProgressDialog()

        val nameStr = names!!.text.toString()
        val companyStr = company!!.text.toString()
        val physicalAddressStr = physical_address!!.text.toString()
        val postalAddressStr = postal_address!!.text.toString()
        val websiteStr = website!!.text.toString()
        val occupationStr = occupation!!.text.toString()


        val params = HashMap<String,String>()
        params.put("name", nameStr)
        params.put("company", companyStr)
        params.put("postal_address", postalAddressStr)
        params.put("physical_address", physicalAddressStr)
        params.put("website", websiteStr)
        params.put("occupation", occupationStr)
        params.put("contact_type", contactStr!!)

        val stringRequest = object : JsonObjectRequest(
            Urls.getUrl(Urls.register_profile), JSONObject(params),
            Response.Listener { response ->
                try {
                    prefs!![Constants.name] = nameStr
                    val message = response.getString("message")
                    runOnUiThread {
                        hideProgressDialog()
                        showToast(message)
                        val intent = Intent( this, UploadImageActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { error ->
                runOnUiThread{hideProgressDialog()}
                val response = error.networkResponse
                if (response != null) {
                    try {
                        val res = String(response.data, Charset.forName(HttpHeaderParser.parseCharset(response.headers, "utf-8")))
                        Log.d("error", res)
                        val obj = JSONObject(res)
                        val message = obj.getString("message")
                        runOnUiThread{showToast(message)}
                    } catch (e1: UnsupportedEncodingException) {
                        e1.printStackTrace()
                    } catch (e2: JSONException) {
                        e2.printStackTrace()
                    }
                }
            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return hashMapOf(
                    "Authorization" to "Bearer "+ prefs!!.getString(Constants.token,Constants.token),
                    "Content-Type" to "application/json"
                )
            }

        }

        VolleyService.requestQueue.add(stringRequest)
    }




    private fun validate(): Boolean {
        var valid = true
        var focusView: View? = null

        val nameStr = names.text.toString()

        if(contactStr.isNullOrEmpty()){
            showToast("Please select contact type")
            focusView = contact_type
            valid = false
        }

        if(nameStr.isEmpty()){
            focusView = names
            valid = false
        }


        if(!valid){
            focusView?.requestFocus()
        }

        return valid
    }


}
