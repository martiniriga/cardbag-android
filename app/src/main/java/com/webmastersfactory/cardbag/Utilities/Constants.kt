package com.webmastersfactory.cardbag.Utilities

import android.text.TextUtils

object Constants {
    const val token= "token"
    const val name = "name"
    const val google_token  = "google_token"
    const val photo_url = "photo_url"
    const val verified_phone  = "verified_phone"
    const val verified_email = "verified_email"
    const val email= "email"
    const val contact = "contact"
    const val contact_id = "contact_id"
    const val phone = "phone"
    const val email_valid = "email_valid"
    const val is_owner = "is_owner"
    const val mytoken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTU0MTU3NjMxNiwibmJmIjoxNTQxNTc2MzE2LCJqdGkiOiJZQXI5RnMySXZsWk5qTmowIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.l2FLVXKd1wTwyzJ2L2Ejx6auBfnT3-_LROOF6QBezPA"


    fun isValidEmail(target: CharSequence): Boolean {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }
}