package com.webmastersfactory.cardbag.Utilities

import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast

fun Context.showToast(message:String, duration:Int =Toast.LENGTH_SHORT){
    Toast.makeText(this,message,duration).show()

}

fun Context.isConnectedToNetwork(): Boolean {
    val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val activeNetwork = connectivityManager.activeNetworkInfo

    val isConnected = activeNetwork != null && activeNetwork.isConnected

    return isConnected
}
