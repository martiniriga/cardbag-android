package com.webmastersfactory.cardbag.models

object Urls {
//    const val APP_URL = "http://10.0.2.2:8000/api/"
//    const val APP_URL = "http://192.168.43.13:81/cardbag-laravel/public/api/"
    private const val APP_URL = "https://cardbag.co/api/"

    const val login = "login"
    const val register = "register"
    const val auth_google = "auth/google"
    const val register_email = "register/email"
    const val register_phone = "register/phone"
    const val upload_photo = "profile/photo"
    const val register_profile = "register/profile"
    const val phone_code = "phone/code"
    const val contacts = "contact"
    const val contacts_personal = "contact/personal"
    const val contacts_list = "contact/list"
    const val share_contact = "contact/share"
    const val add_contact = "contact/add"
    const val delete_contact = "contact/delete/"
    const val my_cards = "contact/me"
    const val profile = "profile"
    const val logout = "logout"

    fun getUrl(url:String):String{
        return APP_URL + url
    }
}