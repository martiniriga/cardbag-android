package com.webmastersfactory.cardbag.models

data class Contact(
    val id: Int? = null,
    val names :String? = null,
    val photo_url :String? = null,
    val company:String? = null,
    val email: String? = null,
    val phone: String? = null,
    val occupation :String? = null,
    val postal_address :String? = null,
    val physical_address :String? = null,
    val website: String? = null,
    val type: String? = null
)