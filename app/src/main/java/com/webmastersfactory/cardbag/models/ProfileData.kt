package com.webmastersfactory.cardbag.models


data class ProfileData (
    var id: Int? = null,
    var name: String? = null,
    var email: String? = null,
    var photo_url: String? = null,
    var phone: String? = null,
    var verified_email: Boolean = false,
    var verified_phone: Boolean = false,
    var token: String? = null
)